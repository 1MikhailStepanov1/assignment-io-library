section .text
 
 
; Принимает код возврата и завершает текущий процесс
;Params:
;	rdi: Return status
;Out:
;	!exit
exit: 
    mov rax, 60
    syscall 


; Принимает указатель на нуль-терминированную строку, возвращает её длину
;Params:
;	rdi: String link
;Out:
;	rax: String length
string_length:
    xor rax, rax		;Очистка rax
    .loop:
    	cmp byte [rdi+rax], 0	;Сравниваем следующий символ с нулем
    	je .end		;Если равенство, прыгаем на часть завершения функции
    	inc rax		;При неравенстве увеличиваем счетчик
    	jmp .loop		;Возвращаемся в начало loop
    .end:
    	ret


; Принимает указатель на нуль-терминированную строку, выводит её в stdout
;Params:
;	rdi: String link
;Out:
;	!stdout
print_string:
	push rdi		;Передали данные для обработки
	call string_length	;Посчитали количество символов для вывода (лежит в rax)
	pop rsi		;Записали строку в rsi
	mov rdx, rax		;Записали количество байтов для вывода
	xor rdi, rdi		;0 в rdi
	inc rdi		;Выставили на stdout
	xor rax, rax		;0 в rax
	inc rax		;Системный код 1 в rax
	syscall
	ret


; Принимает код символа и выводит его в stdout
;Params:
;	rdi: Code of char
;Out:
;	!stdout

print_char:
	push rdi		;Записали символ на стек
	mov rdx, 1		;Положили количество символов для вывода, равное 1
	mov rsi, rsp		;Взяли ссылку на символ, лежащий на стеке
	mov rdi, 1		;Выставили на stdout
	mov rax, 1		;Системный код 1 в rax
	syscall
	pop rdi		
	ret


; Переводит строку (выводит символ с кодом 0xA)
;Params:
;	-
;Out:
;	!stdout

print_newline:
	mov rdi, 0xA		;Кладем символ 0xA в регистр данных
	call print_char	;Вызываем его вывод на экран
	ret
    


; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
;Params:
;	rdi: Unsigned 8-byte decimal number
;Out:
;	!stdout

print_uint:
	mov r10, 10
	mov rax, rdi
	push r12
	mov r12, rsp
	
	dec rsp
	mov byte [rsp], 0
	
	.loop:
		xor rdx, rdx
		div r10
		add rdx, '0'
		dec rsp
		mov byte[rsp], dl
		test rax, rax
		jnz .loop
		
	mov rdi, rsp
	call print_string
	mov rsp, r12
	pop r12
	ret

; Выводит знаковое 8-байтовое число в десятичном формате 
;Params:
;	rdi: Unsigned 8-byte decimal number
;Out:
;	!stdout

print_int:
	cmp rdi, 0		;Сравниваем с rdi с 0
	jge .out		;Если больше, сразу переходим к выводу
	push rdi		;Сохраняем число в стеке
	mov rdi, '-'		;Ставим знак минуса для вывода
	call print_char	;Выводим минус
	pop rdi		;Возвращаем число
	neg rdi		;Делаем его положительным
	.out:			;Выводим положительное число
		call print_uint
	ret


; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
;Params:
;	rdi: First string link
;	rsi: Second string link
;Out:
;	rax: Result

string_equals:
	xor rax, rax			;Очищаем rax
	.loop:				
		mov al, byte [rdi]	;Берем символ из первой строки
		cmp al, byte [rsi]	;Сравниваем символы
		jne .ne		;Переходим в вывод 0, если не равны
		inc rdi		;Перемещаем указатель на след символ в первой строке
		inc rsi		;Перемещаем указатель на след символ во второй строке
		cmp al, 0		;Проверяем строки на окончание
		jne .loop		;Возвращаемся на проверку след символа
		
	.e:				;Вывод 1, если равны
		xor rax, rax
		inc rax
		ret
	.ne:				;Вывод 0, если не равны
		xor rax, rax
		ret


; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
;Params:
;	!stdin
;Out:
;	rax: 	Char from stdin
;		0 if the end of the stream is reached

read_char:
	xor rdi, rdi		;Выставляем на stdin
	xor rdx, rdx		
	inc rdx		;Вводим количество считываемых символов
	xor rax, rax		;Выставляем системный код 0 для считывания и вместе с этим нуль-терминатор для окончания считывания
	push rax		;Кладем нуль-терминатор на стек
	mov rsi, rsp		;сохраняем ссылку на символ
	syscall		
	pop rax		;Возвращаем rax со стека
	ret


; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
;Params:
;	rdi: Buffer string link
;	rsi: Buffer string size   
;Out:
;	rax:	Filled buffer link
;		0 if operation failed
;	rdx: Filled buffer size	

read_word:
	push r12
	push r13
	push r14
	mov r14, rdi ;Start buffer
	mov r12, rsi ;Size buffer
	dec r12 ; null term
	xor r13, r13 ;counter

	.del_spaces:
		call read_char
		cmp rax, 0
		je .end
		cmp rax, 0x20
		je .del_spaces
		cmp rax, 0x9
		je .del_spaces
		cmp rax, 0xA
		je .del_spaces

	.loop:
		cmp r12, r13
		je .bad

		mov byte [r14+r13], al
		inc r13
		call read_char
		
		cmp rax, 0
		je .end
		cmp rax, 0x20
		je .end
		cmp rax, 0x9
		je .end
		cmp rax, 0xA
		je .end
		
		jmp .loop

	.bad:
		xor rax, rax
		pop r14
		pop r13
		pop r12
		ret

	.end:
		mov rax, r14
		mov rdx, r13
		mov byte[r14+r13], 0
		pop r14
		pop r13
		pop r12
		ret
	

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
;Params:
;   	rdi: String link
;Out:
;	rax:	Decimal number length
;	rdx: 	Decimal number length
;		0 if operation failed

parse_uint:
	
	xor rax, rax
	xor r9, r9		;counter
	mov r10, 10		
	xor r11, r11		;digit
	
	.loop:
		mov r11b, byte[rdi]
		cmp r11b, '0'
		jb .digit_end
		cmp r11b, '9'
		ja .digit_end
		mul r10
		sub r11b, '0'
		add rax, r11
		inc rdi
		inc r9
		jmp .loop
	
	.digit_end:
		mov rdx, r9
		ret
	



; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
;Params:
;	rdi: String link
;Out:
;	rdx: 	Number length
;		0 if operation failed

parse_int:
	xor rax, rax
	
	mov al, byte[rdi]
	cmp al, '-'
	jne .read
	
	inc rdi
	push r9
	push r10
	push r11
	call parse_uint
	pop r11
	pop r10
	pop r9
	cmp rdx, 0
	je .error
	neg rax
	inc rdx
	ret
	
	.read:
		call parse_uint
		ret
	.error:
		xor rax, rax
		xor rdx, rdx
		ret


; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
;Params:
;	rdi: String link
;	rsi: Buffer string link
;	rdx: Buffer string size	
;Out:
;	rax:	String length
;		0 if buffer end reached

string_copy:
	push rdi
	push rsi
	push rdx
	call string_length
	pop rdx
	pop rsi
	pop rdi
	cmp rax, rdx
	ja .error
	
	.loop:
		xor r9, r9
		mov r9b, byte[rdi]
		mov byte[rsi], r9b
		inc rdi
		inc rsi
		cmp r9b, 0
		je .end
		jmp .loop
		
	.end:
		ret
			
	.error:
		xor rax, rax
		ret	
